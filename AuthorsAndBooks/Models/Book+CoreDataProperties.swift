//
//  Book+CoreDataProperties.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 1.12.20.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var date: Date
    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var pages: Int32
    @NSManaged public var publisher: String
    @NSManaged public var author: Author

}

extension Book : Identifiable {

}
