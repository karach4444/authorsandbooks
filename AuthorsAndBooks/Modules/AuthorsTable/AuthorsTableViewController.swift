//
//  AuthorsTableViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/11/20.
//

import UIKit
import CoreData

class AuthorsTableViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    private lazy var fetchedResultsController: NSFetchedResultsController<Author> = {
        let fetchRequest: NSFetchRequest<Author> = Author.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        var controller = NSFetchedResultsController<Author>(
            fetchRequest: fetchRequest,
            managedObjectContext: DataStoreManager.shared.viewContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        return controller
    }()
    private var authors = [Author]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let nib = UINib(nibName: String(describing: AuthorTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: AuthorTableViewCell.self))
        performFetch()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc private func addAuthor() {
        let addVC = EditAuthorViewController(nibName: String(describing: EditAuthorViewController.self), bundle: nil)
        addVC.completionHandler = { newAuthor in
            self.performFetch()
        }
        navigationController?.pushViewController(addVC, animated: true)
    }
    
    private func performFetch() {
        do {
            try fetchedResultsController.performFetch()
            authors = fetchedResultsController.fetchedObjects ?? []
            tableView.reloadData()
        } catch let error {
            let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    private func setupView() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAuthor))
        searchBar.delegate = self
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return authors.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AuthorTableViewCell.self), for: indexPath) as! AuthorTableViewCell
        let author = authors[indexPath.item]
        cell.firstName.text = author.firstName
        cell.lastName.text = author.lastName
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataStoreManager.shared.deleteAuthor(with: authors[indexPath.item].id)
            authors.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let author = authors.remove(at: fromIndexPath.item)
        authors.insert(author, at: to.item)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .normal, title: nil) { (_, view, _) in
            let editAuthorVC = EditAuthorViewController(nibName: String(describing: EditAuthorViewController.self), bundle: nil)
            editAuthorVC.author = self.authors[indexPath.item]
            editAuthorVC.completionHandler = { newAuthor in
                self.performFetch()
            }
            self.navigationController?.pushViewController(editAuthorVC, animated: true)
        }
        action.image = UIImage(systemName: "pencil.circle.fill")
        action.backgroundColor = .orange
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let booksTableVC = storyboard!.instantiateViewController(withIdentifier: String(describing: BooksTableViewController.self)) as! BooksTableViewController
        booksTableVC.author = authors[indexPath.item]
        navigationController?.pushViewController(booksTableVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AuthorsTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "(firstName CONTAINS[c] %@) OR (lastName CONTAINS[c] %@)", searchText, searchText)
            performFetch()
        } else {
            fetchedResultsController.fetchRequest.predicate = nil
            performFetch()
        }
    }
}
