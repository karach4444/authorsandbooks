//
//  BookViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/13/20.
//

import UIKit

class BookViewController: BaseViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var pagesLabel: UILabel!
    @IBOutlet weak var pages: UILabel!
    
    var book: Book!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func setupView() {
        name.text = book.name
        author.text = "\(book.author.firstName) \(book.author.lastName)"
        let dateString = Calendar.current.dateComponents([.day, .month, .year], from: book.date)
        date.text = "\(dateString.day!)-\(dateString.month!)-\(dateString.year!)"
        publisher.text = book.publisher
        pages.text = book.pages.description
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
