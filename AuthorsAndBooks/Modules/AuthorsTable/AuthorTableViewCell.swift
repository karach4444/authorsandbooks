//
//  AuthorTableViewCell.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/11/20.
//

import UIKit

class AuthorTableViewCell: UITableViewCell {
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
