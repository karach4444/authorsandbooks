//
//  BooksTableViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/11/20.
//

import UIKit
import CoreData

class BooksTableViewController: UITableViewController {
    @IBOutlet private weak var searchBar: UISearchBar!
    
    var author: Author?
    private lazy var fetchedResultsController: NSFetchedResultsController<Book> = {
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DataStoreManager.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return controller
    }()
    private var books = [Book]()
    private var fetchPredicate: NSPredicate? {
        if let author = author {
            if let name = searchBar.text, name != "" {
                return NSPredicate(format: "(author == %@) AND (name CONTAINS[c] %@)", author, name)
            } else {
                return NSPredicate(format: "author == %@", author)
            }
        } else {
            if let name = searchBar.text, name != "" {
                return NSPredicate(format: "name CONTAINS[c] %@", name)
            } else {
                return nil
            }
        }
    }
    
    @objc private func addBook() {
        let addBookVC =  EditBookViewController(nibName: String(describing: EditBookViewController.self), bundle: nil)
        addBookVC.completionHandler = { book in
            self.performFetch()
        }
        navigationController?.pushViewController(addBookVC, animated: true)
    }
    
    private func performFetch() {
        do {
            fetchedResultsController.fetchRequest.predicate = fetchPredicate
            try fetchedResultsController.performFetch()
            books = fetchedResultsController.fetchedObjects ?? []
            tableView.reloadData()
        } catch let error {
            let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        performFetch()
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tableView.register(UINib(nibName: String(describing: BookTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: BookTableViewCell.self))
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    private func setupView() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBook))
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return books.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BookTableViewCell.self), for: indexPath) as! BookTableViewCell
        let book = books[indexPath.item]
        cell.authorLabel.text = String("\(book.author.firstName) \(book.author.lastName)")
        cell.nameLabel.text = book.name
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .normal, title: nil) { (_, view, _) in
            let editBookVC = EditBookViewController(nibName: String(describing: EditBookViewController.self), bundle: nil)
            editBookVC.book = self.books[indexPath.item]
            editBookVC.completionHandler = { updatedBook in
                self.performFetch()
            }
            self.navigationController?.pushViewController(editBookVC, animated: true)
        }
        action.image = UIImage(systemName: "pencil.circle.fill")
        action.backgroundColor = .orange
        return UISwipeActionsConfiguration(actions: [action])
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataStoreManager.shared.deleteBook(with: books[indexPath.item].id)
            books.remove(at: indexPath.item)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let book = books.remove(at: fromIndexPath.item)
        books.insert(book, at: to.item)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookVC = BookViewController(nibName: String(describing: BookViewController.self), bundle: nil)
        bookVC.book = books[indexPath.item]
        navigationController?.pushViewController(bookVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

extension BooksTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        performFetch()
    }
}
