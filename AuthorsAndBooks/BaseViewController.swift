//
//  ViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/11/20.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        localize()
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        
    }
    
    func localize() { }
}

