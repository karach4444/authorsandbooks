//
//  DataStoreManager.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 1.12.20.
//

import Foundation
import CoreData

class DataStoreManager {
    static let shared = DataStoreManager()
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "AuthorsAndBooks")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var viewContext: NSManagedObjectContext = persistentContainer.viewContext
    
    private init() { }

    // MARK: - Core Data Saving support

    private func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getAllBooks() -> [Book] {
        let fetchRequest = NSFetchRequest<Book>(entityName: String(describing: Book.self))
        if let books = try? viewContext.fetch(fetchRequest) as [Book] {
            return books
        } else {
            return []
        }
    }
    
    func getAllAuthors() -> [Author] {
        let fetchRequest = NSFetchRequest<Author>(entityName: String(describing: Author.self))
        if let authors = try? viewContext.fetch(fetchRequest) {
            return authors
        } else {
            return []
        }
    }
    
    func addAuthor(firstName: String, lastName: String, patronymic: String, birthday: Date, deathDate: Date?, country: String) -> Author? {
        let author = Author(context: viewContext)
        author.id = UUID()
        author.firstName = firstName
        author.lastName = lastName
        author.patronymic = patronymic
        author.birthday = birthday
        author.deathDate = deathDate
        author.country = country
        do {
            try viewContext.save()
            return author
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func addBook(name: String, date: Date, pages: Int32, publisher: String, author: Author) -> Book? {
        let book = Book(context: viewContext)
        book.id = UUID()
        book.name = name
        book.date = date
        book.pages = pages
        book.publisher = publisher
        book.author = author
        do {
            try viewContext.save()
            return book
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func updateAuthor(with id: UUID, firstName: String, lastName: String, patronymic: String, birthday: Date, deathDate: Date?, country: String) -> Author? {
        let fetchReguest = NSFetchRequest<Author>(entityName: String(describing: Author.self))
        fetchReguest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        guard let author = try? viewContext.fetch(fetchReguest).first else {
            return nil
        }
        author.firstName = firstName
        author.lastName = lastName
        author.patronymic = patronymic
        author.birthday = birthday
        author.deathDate = deathDate
        author.country = country
        do {
            try viewContext.save()
            return author
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func updateBook(with id: UUID, name: String, date: Date, pages: Int32, publisher: String, author: Author) -> Book? {
        let fetchReguest = NSFetchRequest<Book>(entityName: String(describing: Book.self))
        fetchReguest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        guard let book = try? viewContext.fetch(fetchReguest).first else {
            return nil
        }
        book.name = name
        book.date = date
        book.pages = pages
        book.publisher = publisher
        book.author = author
        do {
            try viewContext.save()
            return book
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func deleteAuthor(with id: UUID) {
        let fetchReguest = NSFetchRequest<Author>(entityName: String(describing: Author.self))
        fetchReguest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        guard let author = try? viewContext.fetch(fetchReguest).first else {
            return
        }
        viewContext.delete(author)
        try? viewContext.save()
    }
    
    func deleteBook(with id: UUID) -> Bool {
        let fetchReguest = NSFetchRequest<Book>(entityName: String(describing: Book.self))
        fetchReguest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        guard let book = try? viewContext.fetch(fetchReguest).first else {
            return false
        }
        viewContext.delete(book)
        do {
            try viewContext.save()
            return true
        } catch let error {
            print(error.localizedDescription)
            return false
        }
    }
    
    func getAllBooksOfAuthor(with id: UUID) -> [Book]? {
        let fetchReguest = NSFetchRequest<Author>(entityName: String(describing: Author.self))
        fetchReguest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        guard let books = try? viewContext.fetch(fetchReguest).first?.books.allObjects as? [Book] else {
            return nil
        }
        return books
    }
}
