//
//  Author+CoreDataProperties.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 1.12.20.
//
//

import Foundation
import CoreData


extension Author {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Author> {
        return NSFetchRequest<Author>(entityName: "Author")
    }

    @NSManaged public var birthday: Date
    @NSManaged public var country: String
    @NSManaged public var deathDate: Date?
    @NSManaged public var firstName: String
    @NSManaged public var id: UUID
    @NSManaged public var lastName: String
    @NSManaged public var patronymic: String
    @NSManaged public var books: NSSet

}

// MARK: Generated accessors for books
extension Author {

    @objc(addBooksObject:)
    @NSManaged public func addToBooks(_ value: Book)

    @objc(removeBooksObject:)
    @NSManaged public func removeFromBooks(_ value: Book)

    @objc(addBooks:)
    @NSManaged public func addToBooks(_ values: NSSet)

    @objc(removeBooks:)
    @NSManaged public func removeFromBooks(_ values: NSSet)

}

extension Author : Identifiable {

}
