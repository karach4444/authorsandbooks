//
//  AddBookViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/11/20.
//

import UIKit
import CoreData

class EditBookViewController: BaseTextFieldViewController {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var authorTextField: UITextField!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var datePicker: UIDatePicker!
    @IBOutlet private weak var publisherLabel: UILabel!
    @IBOutlet private weak var publisherTextField: UITextField!
    @IBOutlet private weak var pagesLabel: UILabel!
    @IBOutlet private weak var pagesTextField: UITextField!
    @IBOutlet private weak var addButton: UIButton!
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Author> = {
        let fetchRequest: NSFetchRequest<Author> = Author.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DataStoreManager.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return controller
    }()
    private lazy var authors = [Author]()
    private var chosenAuthor: Author?
    var book: Book?
    var completionHandler: ((_ book: Book) -> ())?
    
    @IBAction private func addNewBook() {
        
        guard let name = nameTextField.text, let author = chosenAuthor, let publisher = publisherTextField.text, let stringPages = pagesTextField.text, let pages = Int32(stringPages) else {
            return
        }
        if checkFields() {
            if let prevBook = book {
                if let updatedBook = DataStoreManager.shared.updateBook(with: prevBook.id, name: name, date: datePicker.date, pages: pages, publisher: publisher, author: chosenAuthor!) {
                    completionHandler?(updatedBook)
                    navigationController?.popViewController(animated: true)
                }
            } else {
                if let book = DataStoreManager.shared.addBook(name: name, date: datePicker.date, pages: pages, publisher: publisher, author: author) {
                    completionHandler?(book)
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    override func viewDidLoad() {
        do {
            try fetchedResultsController.performFetch()
            authors = fetchedResultsController.fetchedObjects ?? []
            chosenAuthor = authors.first
        } catch let error {
            let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func setupView() {
        super.setupView()
        datePicker.preferredDatePickerStyle = .compact
        if let firstAuthor = authors.first {
            authorTextField.text = String("\(firstAuthor.firstName) \(firstAuthor.lastName)")
        }
        createPickerView()
        dismissPickerView()
        if let book = book {
            nameTextField.text = book.name
            datePicker.date = book.date
            pagesTextField.text = book.pages.description
            publisherTextField.text = book.publisher
            authorTextField.text = String("\(book.author.firstName) \(book.author.lastName)")
            chosenAuthor = book.author
            addButton.setTitle("Edit", for: .normal)
        }
        nameTextField.delegate = self
        publisherTextField.delegate = self
        pagesTextField.delegate = self
    }
    
    private func checkFields() -> Bool {
        if let name = nameTextField.text, name.count > 2, chosenAuthor != nil, let publisher = publisherTextField.text, publisher.count > 2, let stringPages = pagesTextField.text, let pages = Int32(stringPages), pages > 0 {
            return true
        }
        return false
    }
    
    private func createPickerView() {
           let pickerView = UIPickerView()
           pickerView.delegate = self
           authorTextField.inputView = pickerView
    }
    
    private func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEditing))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
       authorTextField.inputAccessoryView = toolBar
    }
    
    @objc private func endEditing() {
          view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditBookViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return authors.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String("\(authors[row].firstName) \(authors[row].lastName)")
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        authorTextField.text = String("\(authors[row].firstName) \(authors[row].lastName)")
        chosenAuthor = authors[row]
    }
    
    
}
