//
//  AddAuthorViewController.swift
//  AuthorsAndBooks
//
//  Created by Anton Karachinskiy on 11/13/20.
//

import UIKit

class EditAuthorViewController: BaseTextFieldViewController {
    @IBOutlet private weak var firstNameLabel: UILabel!
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameLabel: UILabel!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var patronymicLabel: UILabel!
    @IBOutlet private weak var patronymicTextField: UITextField!
    @IBOutlet private weak var birthdayLabel: UILabel!
    @IBOutlet private weak var birthdayPicker: UIDatePicker!
    @IBOutlet private weak var deathDateLabel: UILabel!
    @IBOutlet private weak var deathDatePicker: UIDatePicker!
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var countryTextField: UITextField!
    @IBOutlet private weak var addButton: UIButton!
    
    var author: Author?
    var completionHandler: ((Author) -> ())?
    
    @IBAction func AddAuthor() {
        guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let patronymic = patronymicTextField.text, let country = countryTextField.text else {
            return
        }
        if checkFields() {
            if let author = author {
                if let author = DataStoreManager.shared.updateAuthor(with: author.id, firstName: firstName, lastName: lastName, patronymic: patronymic, birthday: birthdayPicker.date, deathDate: deathDatePicker.date, country: country) {
                    completionHandler?(author)
                    navigationController?.popViewController(animated: true)
                }
            } else {
                if let author = DataStoreManager.shared.addAuthor(firstName: firstName, lastName: lastName, patronymic: patronymic, birthday: birthdayPicker.date, deathDate: deathDatePicker.date, country: country) {
                    completionHandler?(author)
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        patronymicTextField.delegate = self
        countryTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func setupView() {
        super.setupView()
        birthdayPicker.preferredDatePickerStyle = .compact
        deathDatePicker.preferredDatePickerStyle = .compact
        if let author = author {
            firstNameTextField.text = author.firstName
            lastNameTextField.text = author.lastName
            patronymicTextField.text = author.patronymic
            countryTextField.text = author.patronymic
            birthdayPicker.date = author.birthday
            deathDatePicker.date = author.deathDate ?? Date()
            addButton.setTitle("Edit", for: .normal)
        }
    }
    
    private func checkFields() -> Bool {
        if let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, firstName.count > 3, lastName.count > 3, let patronymic = patronymicTextField.text, patronymic.count > 3, let country = countryTextField.text, country.count > 3, birthdayPicker.date < deathDatePicker.date {
            return true
        }
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
